# Strokes

A small PWA for free-hand drawing.

## TODO

**Vital:**

- True full-screen PWA on Android/iOS. **Blocked:** it is fine with Chrome, but not with Firefox ([bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1813875))
