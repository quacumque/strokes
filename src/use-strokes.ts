import { getStroke } from "perfect-freehand";
import {
  computed,
  reactive,
  Ref,
  shallowReactive,
  shallowRef,
  watch,
} from "vue";

export interface Strokes {
  listeners: {
    pointerdown: (e: PointerEvent) => void;
    pointermove: (e: PointerEvent) => void;
    pointerup: (e: PointerEvent) => void;
  };
  /**
   * Clear current strokes set
   */
  clear: () => void;
  /**
   * Declarative rendering list of strokes
   */
  render: Ref<StrokeRender[]>;
}

interface StrokeRender {
  /**
   * Unique stroke's key for rendering
   */
  key: string | number;
  /**
   * `<path d="<path>" />`
   */
  path: string;
  /**
   * Is this stroke still drawing?
   */
  sliding: boolean;
}

export function useStrokes(): Strokes {
  const completed = reactive<string[]>([]);

  const pending = shallowRef<null | PointTriple[]>(null);
  const pendingPath = computed(() =>
    pending.value ? triplesToPath(pending.value) : null,
  );

  watch(pendingPath, (value, oldValue) => {
    if (!value && oldValue) {
      completed.push(oldValue);
    }
  });

  function onPointerdown(e: PointerEvent) {
    console.log('down!')
    pending.value = shallowReactive([eventToTriple(e)]);
  }

  function onPointermove(e: PointerEvent) {
    if (e.buttons !== 1) return;
    if (pending.value) {
      pending.value.push(eventToTriple(e));
    }
  }

  function onPointerup() {
    pending.value = null;
  }

  function clear() {
    completed.splice(0, completed.length);
  }

  const render = computed((): StrokeRender[] => {
    const completedMapped = completed.map((d, i) => ({
      path: d,
      key: i,
      sliding: false,
    }));
    if (!pendingPath.value) return completedMapped;
    return [
      ...completedMapped,
      { path: pendingPath.value, sliding: true, key: completedMapped.length },
    ];
  });

  return {
    listeners: {
      pointerdown: onPointerdown,
      pointermove: onPointermove,
      pointerup: onPointerup,
    },
    clear,
    render,
  };
}

type PointTriple = [x: number, y: number, pressure: number];

function getSvgPathFromStroke(stroke: number[][]) {
  if (!stroke.length) return "";

  const d = stroke.reduce(
    (acc, [x0, y0], i, arr) => {
      const [x1, y1] = arr[(i + 1) % arr.length];
      acc.push(x0, y0, (x0 + x1) / 2, (y0 + y1) / 2);
      return acc;
    },
    ["M", ...stroke[0], "Q"],
  );

  d.push("Z");
  return d.join(" ");
}

function eventToTriple(e: PointerEvent): PointTriple {
  return [e.offsetX, e.offsetY, e.pressure];
}

function triplesToPath(points: PointTriple[]): string {
  const stroke = getStroke(points, {
    size: 8,
    thinning: 0.1,
    smoothing: 0.5,
    streamline: 0.75,
  });

  return getSvgPathFromStroke(stroke);
}
