import { createApp } from "vue";
import App from "./App.vue";

export function create(rootContainer: string | Element) {
  createApp(App).mount(rootContainer);
}
